const { decode } = require("jsonwebtoken");
const pool = require("../../config/database");

module.exports = {
  //Login
  getUserByUserName: (userName, callBack) => {
    pool.query(
      "select FK_CompanyID,Password from ShuttlepathCorporateDB.TB_User where UserName = ? AND ? between ValidFrom and ValidTo ",
      [userName, new Date()],
      (error, results, fields) => {
        if (error) {
          callBack(error);
        }
        return callBack(null, results[0]);
      }
    );
  },
};
