const { create, getUserByUserName } = require("./userservice");
const { compareSync } = require("bcrypt");
const { sign } = require("jsonwebtoken");

module.exports = {
  //User Login
  login: (req, res) => {
    const body = req.body;
    console.log("Body", body);
    getUserByUserName(body.UserName, (err, results) => {
      if (!results) {
        return res.json({
          message: "Invalid Username,Password or User Unavailable",
        });
      }

      //Check User Password
      const result = compareSync(body.Password, results.Password);
      if (result) {
        result.Password = undefined;
        const jsonwebtoken = sign({ result: results }, "abc123", {
          expiresIn: "1h",
        });
        return res.json({
          message: "login Successfully",
          token: jsonwebtoken,
        });
      } else {
        return res.json({
          message: "Invalid Username or Password",
        });
      }
    });
  },
};
