const { login } = require("./usercontroller");
const router = require("express").Router();
const { checkToken } = require("../../auth/tokenValidation");

router.post("/", checkToken);
router.post("/login", login);

module.exports = router;
